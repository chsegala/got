# -*- encoding : utf-8 -*-
module Properties
  class System
    def initialize
      @@config = YAML.load_file("#{Rails.root}/config/system.yml")
    end

    def [](key)
      (@@config[Rails.env] || @@config['test'] || @@config['development'] || @@config['production'])[key]
    end
  end
end
