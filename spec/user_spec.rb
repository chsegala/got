# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Users::OmniauthCallbacksController, :type => :controller do
  it "should create a disabled user sign up" do
    @request.env["devise.mapping"] = Devise.mappings[:admin]
    User.all.size.should be_equal 0
    get :facebook, {}

    #User.all.size.should be_equal 1
  end
end
