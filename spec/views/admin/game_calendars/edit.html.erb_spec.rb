# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "admin/game_calendars/new" do
  before(:each) do
    assign(:admin_game_calendar, stub_model(Admin::GameCalendarController,
      :location => "MyString",
      :map => "MyString",
      :instructions => "MyString",
      :title => "MyString",
      :provider => "MyString"
    ).as_new_record)
  end

  it "renders new admin_game_calendar form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", admin_game_calendars_path, "post" do
      assert_select "input#admin_game_calendar_location[name=?]", "admin_game_calendar[location]"
      assert_select "input#admin_game_calendar_map[name=?]", "admin_game_calendar[map]"
      assert_select "input#admin_game_calendar_instructions[name=?]", "admin_game_calendar[instructions]"
      assert_select "input#admin_game_calendar_title[name=?]", "admin_game_calendar[title]"
      assert_select "input#admin_game_calendar_provider[name=?]", "admin_game_calendar[provider]"
    end
  end
end
