# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "admin/game_calendars/show" do
  before(:each) do
    @admin_game_calendar = assign(:admin_game_calendar, stub_model(Admin::GameCalendarController,
      :location => "Location",
      :map => "Map",
      :instructions => "Instructions",
      :title => "Title",
      :provider => "Provider"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Location/)
    rendered.should match(/Map/)
    rendered.should match(/Instructions/)
    rendered.should match(/Title/)
    rendered.should match(/Provider/)
  end
end
