# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "admin/game_calendars/index" do
  before(:each) do
    assign(:admin_game_calendars, [
      stub_model(Admin::GameCalendarController,
        :location => "Location",
        :map => "Map",
        :instructions => "Instructions",
        :title => "Title",
        :provider => "Provider"
      ),
      stub_model(Admin::GameCalendarController,
        :location => "Location",
        :map => "Map",
        :instructions => "Instructions",
        :title => "Title",
        :provider => "Provider"
      )
    ])
  end

  it "renders a list of admin/game_calendars" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Location".to_s, :count => 2
    assert_select "tr>td", :text => "Map".to_s, :count => 2
    assert_select "tr>td", :text => "Instructions".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Provider".to_s, :count => 2
  end
end
