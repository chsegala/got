require 'spec_helper'

describe "admin/camps/new" do
  before(:each) do
    assign(:admin_camp, stub_model(Admin::Camp,
      :name => "MyString",
      :description => "MyString",
      :picture => "MyString",
      :map => "MyString"
    ).as_new_record)
  end

  it "renders new admin_camp form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", admin_camps_path, "post" do
      assert_select "input#admin_camp_name[name=?]", "admin_camp[name]"
      assert_select "input#admin_camp_description[name=?]", "admin_camp[description]"
      assert_select "input#admin_camp_picture[name=?]", "admin_camp[picture]"
      assert_select "input#admin_camp_map[name=?]", "admin_camp[map]"
    end
  end
end
