require 'spec_helper'

describe "admin/camps/index" do
  before(:each) do
    assign(:admin_camps, [
      stub_model(Admin::Camp,
        :name => "Name",
        :description => "Description",
        :picture => "Picture",
        :map => "Map"
      ),
      stub_model(Admin::Camp,
        :name => "Name",
        :description => "Description",
        :picture => "Picture",
        :map => "Map"
      )
    ])
  end

  it "renders a list of admin/camps" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Picture".to_s, :count => 2
    assert_select "tr>td", :text => "Map".to_s, :count => 2
  end
end
