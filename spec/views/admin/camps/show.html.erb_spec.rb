require 'spec_helper'

describe "admin/camps/show" do
  before(:each) do
    @admin_camp = assign(:admin_camp, stub_model(Admin::Camp,
      :name => "Name",
      :description => "Description",
      :picture => "Picture",
      :map => "Map"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Description/)
    rendered.should match(/Picture/)
    rendered.should match(/Map/)
  end
end
