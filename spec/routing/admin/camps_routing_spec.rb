require "spec_helper"

describe Admin::CampsController do
  describe "routing" do

    it "routes to #index" do
      get("/admin/camps").should route_to("admin/camps#index")
    end

    it "routes to #new" do
      get("/admin/camps/new").should route_to("admin/camps#new")
    end

    it "routes to #show" do
      get("/admin/camps/1").should route_to("admin/camps#show", :id => "1")
    end

    it "routes to #edit" do
      get("/admin/camps/1/edit").should route_to("admin/camps#edit", :id => "1")
    end

    it "routes to #create" do
      post("/admin/camps").should route_to("admin/camps#create")
    end

    it "routes to #update" do
      put("/admin/camps/1").should route_to("admin/camps#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/admin/camps/1").should route_to("admin/camps#destroy", :id => "1")
    end

  end
end
