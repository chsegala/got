# -*- encoding : utf-8 -*-
require "spec_helper"

describe Admin::GameCalendarsController do
  describe "routing" do

    it "routes to #index" do
      get("/admin/game_calendars").should route_to("admin/game_calendars#index")
    end

    it "routes to #new" do
      get("/admin/game_calendars/new").should route_to("admin/game_calendars#new")
    end

    it "routes to #show" do
      get("/admin/game_calendars/1").should route_to("admin/game_calendars#show", :id => "1")
    end

    it "routes to #edit" do
      get("/admin/game_calendars/1/edit").should route_to("admin/game_calendars#edit", :id => "1")
    end

    it "routes to #create" do
      post("/admin/game_calendars").should route_to("admin/game_calendars#create")
    end

    it "routes to #update" do
      put("/admin/game_calendars/1").should route_to("admin/game_calendars#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/admin/game_calendars/1").should route_to("admin/game_calendars#destroy", :id => "1")
    end

  end
end
