class Camp < ActiveRecord::Base
  attr_accessible :description, :map, :name, :picture

  validates_presence_of :name, :description

  has_attached_file :picture,
                    :storage => :dropbox,
                    :dropbox_credentials => Properties::System.new()['dropbox']
  validates_attachment :picture, :presence => true,
                       :content_type => { :content_type => ["image/jpg", "image/png"] },
                       :size => { :in => 0..300.kilobytes }

end
