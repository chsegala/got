# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :omniauthable, :omniauth_providers => [:facebook]

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :enabled, :name, :provider, :uid, :username, :facebook_link
  has_and_belongs_to_many :roles

  def self.find_for_facebook_oauth(auth, current_user = nil)
    user = User.find_by_provider_and_uid(auth.provider, auth.uid)
    if user and not user.facebook_link
      user.facebook_link = auth.info.urls.Facebook
      user.save
    end
    unless user
      user = User.create(name:auth.info.name, provider:auth.provider, uid:auth.uid, email:auth.info.email,
                         enabled:false, facebook_link:auth.info.urls.Facebook)
    end
    user
  end

  def role? role
    role = [role] unless role.is_a? Array
    roles.each { |r| return true if role.include? r.name  }
    false
  end
end
