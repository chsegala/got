# -*- encoding : utf-8 -*-
class GameCalendar < ActiveRecord::Base
  attr_accessible :date, :instructions, :location, :map, :provider, :title

  validates_presence_of :instructions, :location, :title
end
