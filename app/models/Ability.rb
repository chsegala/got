# -*- encoding : utf-8 -*-
class Ability
  include CanCan::Ability

  def initialize(user, controller_namespace)
    user ||= User.new # guest user

    case controller_namespace
      when "Admin"
        can :manage, :all if user.role? "Administrador"
      else
        if user.role? "Administrador"
          can :manage, :all
        elsif user.role? "Usuário"
          can :read, :all
        end
    end
  end
end
