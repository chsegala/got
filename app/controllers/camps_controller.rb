class CampsController < ApplicationController
  def index
    @camps = Camp.all
    @markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, :autolink => true, :space_after_headers => true)
  end
end