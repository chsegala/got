# -*- encoding : utf-8 -*-
class Admin::GameCalendarsController < Admin::AdminController
  # GET /admin/game_calendars
  # GET /admin/game_calendars.json
  def index
    @game_calendars = GameCalendar.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @game_calendars }
    end
  end

  # GET /admin/game_calendars/new
  # GET /admin/game_calendars/new.json
  def new
    @game_calendar = GameCalendar.new

    respond_to do |format|
      format.html { render "edit" } # edit.html.erb
      format.json { render json: @game_calendar }
    end
  end

  # GET /admin/game_calendars/1/edit
  def edit
    @game_calendar = GameCalendar.find(params[:id])
  end

  # POST /admin/game_calendars
  # POST /admin/game_calendars.json
  def create
    @game_calendars = GameCalendar.new(params[:game_calendar])

    respond_to do |format|
      if @game_calendars.save
        format.html { redirect_to admin_game_calendars_path, notice: 'Game calendar was successfully created.' }
        format.json { render json: @game_calendars, status: :created, location: @game_calendars }
      else
        format.html { render action: "new" }
        format.json { render json: @game_calendars.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/game_calendars/1
  # PUT /admin/game_calendars/1.json
  def update
    @game_calendars = GameCalendar.find(params[:id])

    respond_to do |format|
      if @game_calendars.update_attributes(params[:game_calendar])
        format.html { redirect_to admin_game_calendars_path, notice: 'Game calendar was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @game_calendars.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/game_calendars/1
  # DELETE /admin/game_calendars/1.json
  def destroy
    @game_calendars = GameCalendar.find(params[:id])
    @game_calendars.destroy

    respond_to do |format|
      format.html { redirect_to admin_game_calendars_url }
      format.json { head :no_content }
    end
  end
end
