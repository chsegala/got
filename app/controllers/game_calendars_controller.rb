# -*- encoding : utf-8 -*-
class GameCalendarsController < ApplicationController
  def index
    @game_calendars = GameCalendar.where("date > ?", Date.current - 1.years)
  end

  def show
    @game_calendar = GameCalendar.find(params[:id])
    @markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, :autolink => true, :space_after_headers => true)
  end
end
