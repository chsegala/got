# -*- encoding : utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#encoding utf-8

admin = Role.create(name:"Administrador")
Role.create(name:"Usuário")
Role.create(name:"Visitante")
User.create(name:"Christian Segala", email:"chsegala@gmail.com", provider:"facebook", uid:"100000952406215",
            enabled:true, facebook_link:"https://www.facebook.com/chsegala")

User.find_by_email("chsegala@gmail.com").roles.push(admin)
