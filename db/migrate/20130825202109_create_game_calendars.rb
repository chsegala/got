# -*- encoding : utf-8 -*-
class CreateGameCalendars < ActiveRecord::Migration
  def change
    create_table :game_calendars do |t|
      t.datetime :date
      t.string :location, :length => 150
      t.string :map, :length => 500
      t.string :instructions
      t.string :title, :length => 150
      t.string :provider, :length => 150

      t.timestamps
    end

    add_index(:game_calendars, :date, :order => { :date => :desc})
  end
end
