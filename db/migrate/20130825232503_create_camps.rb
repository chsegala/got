class CreateCamps < ActiveRecord::Migration
  def change
    create_table :camps do |t|
      t.string :name, :length => 200
      t.string :description
      t.string :map, :length => 500

      t.timestamps
    end
    add_attachment(:camps, :picture)
  end
end
