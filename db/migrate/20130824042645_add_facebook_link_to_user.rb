# -*- encoding : utf-8 -*-
class AddFacebookLinkToUser < ActiveRecord::Migration
  def change
    add_column :users, :facebook_link, :string
    add_index :users, :facebook_link, :unique => true
  end
end
